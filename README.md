# Voro++ Nuget #

This repository contains all files and information to create a NuGet package from the source code of Voro++.

For problems with the NuGet package contact SGrottel: 

* https://bitbucket.org/sgrottel_nuget/voro
* http://www.sgrottel.de
* http://go.sgrottel.de/nuget/voro++

For problems with the library itself contact the author:

* Chris Rycroft
* http://math.lbl.gov/voro++/
* http://people.seas.harvard.edu/~chr/

## Voro++ ##

[Voro++](http://math.lbl.gov/voro++/) is a software library for carrying out three-dimensional computations of the Voronoi tessellation.

A distinguishing feature of the Voro++ library is that it carries out cell-based calculations, computing the Voronoi cell for each particle individually. It is particularly well-suited for applications that rely on cell-based statistics, where features of Voronoi cells (eg. volume, centroid, number of faces) can be used to analyze a system of particles.

Voro++ comprises of several C++ classes that can be built as a static library. A command-line utility is also provided that can use most features of the code. The direct cell-by-cell construction makes the library particularly well-suited to handling special boundary conditions and walls. It employs algorithms that are tolerant for numerical precision errors, it exhibits high performance, and it has been successfully employed on very large particle systems.

This project is free, open-source software, released through the Lawrence Berkeley Laboratory and the US Department of Energy. It is distributed under a modified BSD license, that makes it free for any purpose. The full text of the license is distributed with the code. Any questions about licensing should be directed to the LBL [Tech Transfer](http://www.lbl.gov/Tech-Transfer/) department.

Project Website: http://math.lbl.gov/voro++/

## Update Voro++ ##

* Download the newest code
* Clone a working copy of this repository into a subdirectory, e.g. `msvc`.
* Update the source file list in the Visual Studio Project based on the updated source code.
* Update `voro++.autopkg` commands for packing `include` and `doc`umentation files.
* Also update meta data like version number

Most likely this is all there is.

## Building the NuGet Package ##

* Use the Visual Studio Solution to build the Dlls for all desired (and installed) platform toolsets
* Edit `voro++.autopkg` to reflect your built binaries

Install CoApp Tools: http://coapp.org/

* http://coapp.org/tutorials/installation.html
* http://coapp.org/tutorials/building-a-package.html

Navigate your Powershell to the repository.

Run: `Write-NuGetPackage .\voro++.autopkg`
